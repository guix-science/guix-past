;;; Guix Past --- Packages from the past for GNU Guix.
;;; Copyright © 2024 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This file is part of Guix Past.
;;;
;;; Guix Past is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guix Past is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guix Past.  If not, see <http://www.gnu.org/licenses/>.

(define-module (past packages cran)
  #:use-module (gnu packages)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build-system r)
  #:use-module (gnu packages cran)
  #:use-module (srfi srfi-1))

(define-public r-seuratobject
  (package
    (name "r-seuratobject")
    (version "4.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "SeuratObject" version))
       (sha256
        (base32
         "1vqhad76hnr0l47klg56fniah4cvpzbkkl0j1cn9cp3rv6d69rvl"))))
    (properties `((upstream-name . "SeuratObject")))
    (build-system r-build-system)
    (propagated-inputs
     (map specification->package
          (list "r-future"
                "r-future-apply"
                "r-matrix"
                "r-progressr"
                "r-rcpp"
                "r-rcppeigen"
                "r-rlang"
                "r-sp")))
    (home-page "https://satijalab.org/seurat")
    (synopsis "Data structures for single cell data")
    (description
     "This package defines S4 classes for single-cell genomic data and
associated information, such as dimensionality reduction embeddings,
nearest-neighbor graphs, and spatially-resolved coordinates.  It provides data
access methods and R-native hooks to ensure the Seurat object is familiar to
other R users.")
    (license license:gpl3)))

(define-public r-seurat
  (package
    (name "r-seurat")
    (version "4.4.0")
    (source (origin
              (method url-fetch)
              (uri (cran-uri "Seurat" version))
              (sha256
               (base32
                "1pdlvjh5xs1fyj2zic98sfz9zazbb45qywfqnhfzqb34jyaxy5qg"))))
    (properties `((upstream-name . "Seurat")))
    (build-system r-build-system)
    (propagated-inputs
     (map specification->package
          (list "r-cluster"
                "r-cowplot"
                "r-fitdistrplus"
                "r-future"
                "r-future-apply"
                "r-ggplot2"
                "r-ggrepel"
                "r-ggridges"
                "r-httr"
                "r-ica"
                "r-igraph"
                "r-irlba"
                "r-jsonlite"
                "r-kernsmooth"
                "r-leiden"
                "r-lmtest"
                "r-mass"
                "r-matrix"
                "r-matrixstats"
                "r-miniui"
                "r-patchwork"
                "r-pbapply"
                "r-plotly"
                "r-png"
                "r-progressr"
                "r-purrr"
                "r-rann"
                "r-rcolorbrewer"
                "r-rcpp"
                "r-rcppannoy"
                "r-rcppeigen"
                "r-rcppprogress"
                "r-reticulate"
                "r-rlang"
                "r-rocr"
                "r-rtsne"
                "r-scales"
                "r-scattermore"
                "r-sctransform"
                "r-seuratobject@4"
                "r-shiny"
                "r-spatstat-explore"
                "r-spatstat-geom"
                "r-tibble"
                "r-uwot")))
    (home-page "https://www.satijalab.org/seurat")
    (synopsis "Seurat is an R toolkit for single cell genomics")
    (description
     "This package is an R package designed for QC, analysis, and
exploration of single cell RNA-seq data.  It easily enables widely-used
analytical techniques, including the identification of highly variable genes,
dimensionality reduction; PCA, ICA, t-SNE, standard unsupervised clustering
algorithms; density clustering, hierarchical clustering, k-means, and the
discovery of differentially expressed genes and markers.")
    (license license:gpl3)))
