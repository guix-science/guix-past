;;; Guix Past --- Functional package management for GNU Guix.
;;; Copyright © 2023 Maxim Cournoyer <maxim.cournoyer@gmail.com>
;;;
;;; This file is part of Guix Past.
;;;
;;; Guix Past is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guix Past is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guix Past.  If not, see <http://www.gnu.org/licenses/>.

(define-module (past packages build-tools)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages python)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages xml))

(define-public bakefile
  (package
    (name "bakefile")
    ;; XXX: Use the "legacy" branch, which is still used and supported by
    ;; wxWidgets, *and* has a proper build system (Autotools) and no bundled
    ;; jar.
    (version "0.2.13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/vslavik/bakefile")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "1mx3i4lywri9vg7p89r9768gjzqi752zjcrg9nzipgdj0r28yq8l"))
              ;; Clean up the source from random binaries.
              (snippet '(delete-file "build/mac-python2.3.tar.gz"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:configure-flags #~(list "--enable-maintainer-mode")
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'install-html-doc
            (lambda _
              (let ((doc (string-append #$output "/share/doc/bakefile")))
                (mkdir-p doc)
                (copy-recursively "doc/html" doc)))))))
    ;; No Python 3 support on the horizon (see:
    ;; https://github.com/vslavik/bakefile/issues/109).
    (native-inputs
     (list autoconf
           automake
           docbook-xml-4.1.2
           docbook-xsl
           libtool
           libxml2                      ;for XML_CATALOG_FILES
           libxslt
           python-2
           swig))
    (inputs (list libxcrypt))
    (home-page "https://bakefile.org/")
    (synopsis "Native makefiles generator")
    (description "Bakefile takes compiler-independent description of build
tasks as its input and outputs native makefile(s) such as that of GNU Make,
Visual C++ project, etc.")
    (license license:expat)))
